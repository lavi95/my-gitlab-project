const chai = require('chai');
const expect = chai.expect;
const { promisify } = require('util');
const auth = require('../../../authentication');
const apiRequest = require('../../../apiRequest');
const api = new apiRequest(auth);
const config = require('../../../config/config');
const dropDBP = promisify(
  require('../../../dropMongoDBDatabase').execute,
);

describe('controller', async () => {
  before(async function () {
    await dropDBP(config.MONGO_DB_NAME);
  });

  describe('#Api', async () => {
    it('should save user data to Rave.basic_test ', async function () {
      const requestBody = {
        first_name : "Ben",
        last_name : "Efron",
        email : "BenEfron@gmail.com",
        age : 26
      };
      const res = await api.postRequest(`${config.WEB_URL}/api/v1/saveUsersData?password=2hjh324`, requestBody);
      expect(res.body, 'incorrect').to.be.equal(runQuery());
    });
  });

  function runQuery(){
      return getMockResults("select * from Rave.basic_test");
  }
});
